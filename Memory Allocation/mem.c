//.malloc()	Memory Allocation ==      ptr = (int*)malloc(num*sizeof(data-type)) each block of memory can not be initilize by zero
//malloc use single memory block to assign memory


//calloc()  contigious allocation ==  ptr = (int*)calloc(byte-size, element-size) each block of memory initilize by zero
//calloc use multiple memory block to assign memory



//free()	free memory
//realloc()  re allocate memory


// Remember all Memory allocation function accept the size in bytes and assign new memory in a heap

#include <stdio.h>
#include <stdlib.h>

int main(){
	
	int *ptr,*ptr1;
	int num, sum=0;


	num = 5;
	printf("Assigning the memory \n");

	ptr = (int*)malloc(sizeof(int)*num);

	ptr1 = (int*)calloc(num, sizeof(int));

	if(ptr == NULL || ptr1 == NULL){
		printf("No memory Allocated");
		exit(1);
	}

	printf("Memory is Allocated\n");

	int i=0;
	for(i=0; i< num; i++){
		ptr[i] = i;
		ptr1[i] = i;
	}


	for(i=num-1; i>=0; i--){
		printf("%d\t %d\n",ptr[i],ptr1[i]);
	}

	free(ptr);
	free(ptr1);
	printf("\nMemory Freed\nCheck Free memory\n");
	for(i=num-1; i>=0; i--){
		printf("%d\t %d\n",ptr[i],ptr1[i]);
	}

	return 0;
}