// /dev
//statvfs()  return -1 if error
//fstatvfs()  how system will identify

#include<stdio.h>
#include<sys/statvfs.h>

int main(){

	struct statvfs buff;


	if(statvfs(".",&buff) == -1){
		perror("error");
		exit(1);
	}
	
	printf("Each BLock has size of %ld bytes\n",buff.f_frsize);


	printf("Thear are %ld Blocks avalible out of %ld\n",buff.f_bavail, buff.f_blocks);

	return 0;

}




