//Signal Handlers


#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>



void sighandler(int);


int main(){
	

	signal(SIGINT, sighandler);

	for(;;){

		printf("Sleeping.....\n");

		sleep(3); //sleeping 3 sec

		//press ctrl+c while runing
	}


	return 0;
}



void sighandler(int signum){

	printf("Caught Signal %d and i am exiting...\n",signum);       //remember every signal has specific number
	exit(1);

}