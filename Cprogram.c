#include <stdio.h>
#include <stdlib.h>

int main(){

	int num;
	FILE *fptr;
	fptr = fopen("file1.txt","w");  //wb for writing in binary mode

	if(fptr == NULL){
		printf("Error");
		exit(1);
	}

	printf("ENter Num: ");
	scanf("%d",&num);
	char s[10] = {'ABCDEFGHIJ'};

	fprintf(fptr,"%d",num);//used to write in file
	fprintf(fptr,"%s",s);//not write any thing in file
	fclose(fptr);

	return 0;
}