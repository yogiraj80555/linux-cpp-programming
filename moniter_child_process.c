//monitering child process
//parent optaning status of child and also can terminate child process


#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>


int main(){
	
	pid_t cpid;

	if(fork() == 0){
		exit(0);
	}else{

		cpid = wait(NULL);
	}

	printf("Parent process id (pid) : %d\n", getpid());
	printf("Child process id (cpid) : %d\n", cpid);



	return 0;
}