//we can optaine notification when events occurs like files are open, close, created, deleted, modified,rename and so on
//known as Monitiring file events use inotify file notification
//we have to specify a path for this kind of monitoring



#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<sys/types.h>
#include<sys/inotify.h>
#include<unistd.h>


#define EVENT_SIZE (sizeof(struct inotify_event))
#define BUF_LEN (1024 * (EVENT_SIZE + 16))


int main(){
	
	int length, i = 0;
	int fd, wd;

	char buffer[BUF_LEN];

	fd = inotify_init();

	if(fd < 0){
		perror("inotify_init");
	}
 

	wd = inotify_add_watch(fd, "created_for_moniter", IN_MODIFY | IN_CREATE | IN_DELETE);
	//please crete "created_for_moniter" folder before runing the program
	length = read(fd, buffer,BUF_LEN);


	if(length < 0){
		perror("Read error");
	}

	while(i < length){
		struct inotify_event *event = (struct inotify_event *) &buffer[i];
		if(event->mask & IN_CREATE){
			printf("Something Created %s\n",event->name);
		}

		if(event->mask & IN_MODIFY){
			printf("Something Modify %s\n",event->name);
		}

		if(event->mask & IN_DELETE){
			printf("Something Delete %s\n",event->name);
		}


		i+=EVENT_SIZE + event->len;


	}

	(void) inotify_rm_watch(fd, wd);
	(void) close(fd);




	return 0;
}