//process is intance of executing program
// process divided in 4 steps initilize/uninitilized data, stack, data,heap
//command line argument supplied are avalible via avgs and argv variable

#include <stdio.h>
#include <unistd.h>

int main(){

	int process_id, parent_precess_id;

	process_id = getpid();
	parent_precess_id = getppid();

	printf("Process Id: %d\nParrent process id: %d\n", process_id, parent_precess_id);
}

