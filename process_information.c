//process in /proc/pid  which provides range of information
//cat /proc/1/status
// uname() :- return information about the host system on which application is running -1 it means erro  r


#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<sys/utsname.h>



int main(){
	

	struct utsname buff;

	errno = 0;

	if(uname(&buff) != 0){
		perror("uname Not return 0  so error produce");
		exit(EXIT_FAILURE);
	}


	printf("System name = %s \n", buff.sysname);
	printf("Node name = %s \n", buff.nodename);
	printf("Release  = %s \n", buff.release);
	printf("Version name = %s \n", buff.version);
	printf("Machine name = %s \n", buff.machine);



	return 0;
}