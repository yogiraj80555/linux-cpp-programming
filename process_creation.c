//fork() sys call in process creation it creates a new child process
//the fork create exact duplicate parent process
//there is also exit() and _exit()
//so fork() is duplicate the current process and create exact copy of current process

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>

int main(){
	
	fork();
	printf("Linux Hello!\n");

	//examine above code add more fork() and observe output


	printf("Linux Hello!\n");

	exit(0);
	//the difference between _exit() and exit() is  
	//the exit() function is perform some cleaning before the termination of programme like connection termination buffer cleaning etc.
	// where as _exit() perform normal termination without cleaning any of the task.

	printf("Linux says By!\n");

	return 0;
}