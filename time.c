//2 time real time and process time
//amount of cpu time using for process is known process time

#include <stdio.h>

#include <time.h>



void func(){
	printf("Funtion stats\n");
	printf("press any char to stop");
	for(;;){
		if(getchar())
			break;
	}

	printf("\nFunction Ends\n");
}




int main(){
	
	clock_t t;
	t=clock();
	func();

	t = clock() - t;

	double  time = ((double)t)/CLOCKS_PER_SEC;

	printf("%f time taken by function \n",time);


	
	return 0;
}